import csv

from exceptions import MissingRoleException, MissingSportException, UnsupportedRoleException, UnsupportedSportException
import strategies

def classify_template(winner):
    
    # call the classifier factory to retrieve the strategy needed 
    Strategy = strategies.classification_strategy(winner[3])
    
    # instantiate the classifier
    classifier = Strategy(winner)
    
    # issue the award (i.e. the whole purpose of this)
    award = classifier.award()
    
    # if the above worked correctly, audit what happened.
    # We always want to audit on success so it happens as part of the template, not the calling method
    classifier.audit()
    
    return award

# log message specific to classifier errors
def classify_audit(winner, error_message):
    log = {
            "type" : "failure",
            "id" : winner[0],
            "role" : winner[4],
            "sport" : winner[3],        
            "error_message" : repr(error_message)
        }
    print("Logging: %s" % log)

# main working class
def classify(winner):
    try:
        # This message is just for us to follow along 
        print("Record found: %s" % winner[0])
        classify_template(winner)        
    except (UnsupportedSportException, MissingSportException, UnsupportedRoleException, MissingRoleException) as classify_exception:
        # log out the issue
        classify_audit(winner, classify_exception)
        
def main():
    print("Starting classification engine")
    with open('MOCK_DATA.csv', mode ='r')as file:
        winners = csv.reader(file)
        for winner in winners:
            try:
                classify(winner)
            except Exception as error:
                print("General issue occured: ", error)

if __name__ == "__main__":
    main()

