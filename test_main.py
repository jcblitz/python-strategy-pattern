# content of test_sample.py

import strategies

def test_volleyball_mvp():
    winner = [1,"Gina","Coetzee","Volleyball","Player"]
    
    classifer = strategies.VolleyballClassifier(winner)
    assert classifer.award() == "Most Valuable Player"
