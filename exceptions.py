class UnsupportedSportException(Exception):
    pass

class MissingSportException(Exception):
    pass

class UnsupportedRoleException(Exception):
    pass

class MissingRoleException(Exception):
    pass