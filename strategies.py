import sys
from exceptions import MissingRoleException, MissingSportException, UnsupportedRoleException, UnsupportedSportException

class BaseClassifier:
    def __init__(self, winner):
        self.winner = winner
    
    version = "6dcb09b5b57875f334f61aebed695e2e4193db5e"

    def audit(self):
        log = {
            "type" : "success",
            "id" : self.winner[0],
            "role" : self.winner[4],
            "sport" : self.winner[3],
            "strategy" : self.__class__.__name__,
            "version" : self.version,            
        }
        print("Logging: %s" % log)

    def award(self):
        award_given = ""
        if (self.winner[4] == None or self.winner[4] == ""):
            raise MissingRoleException("Role not provided")
        elif (self.winner[4] == "Player"):
            award_given = "Most Valuable Player"
        elif (self.winner[4] == "Coach"):
            award_given = "Coach of the Year"
        else:
            raise UnsupportedRoleException(self.winner[4])

        return award_given

class SoccerClassifier(BaseClassifier):
    version = "a0f1490a20d0211c997b44bc357e1972deab8ae3"

    def award(self):
        if (self.winner[4] == None or self.winner[4] == ""):
            raise MissingRoleException("Role not provided")
        elif (self.winner[4] == "Player"):
            return "Most Valuable Player"
        elif (self.winner[4] == "Coach"):
            return "Coach of the Year"
        elif (self.winner[4] == "Staff"):
            return "Supporter of the Year"
        else:
            raise UnsupportedRoleException(self.winner[4])

class FootballClassifier(BaseClassifier):
    pass

## Purposely remove this classification to have an issue 
# class BasketballClassifier(BaseClassifier):
#     pass

class BaseballClassifier(BaseClassifier):
    version = "7d13d60bb70da027746a10a54794274b6e189f67"

    def award(self):
        if (self.winner[4] == None or self.winner[4] == ""):
            raise MissingRoleException("Role not provided")
        elif (self.winner[4] == "Player"):
            return "Golden Glove"
        elif (self.winner[4] == "Coach"):
            return "Coach of the Year"
        else:
            raise UnsupportedRoleException(self.winner[4])

class CricketClassifier(BaseClassifier):
    pass

class HockeyClassifier(BaseClassifier):
    pass

class VolleyballClassifier(BaseClassifier):
    pass

# returns a strategy objected used to classify the winner of the award based on their specific sport
def classification_strategy(sport):
    # The sport is the key for determining the strategy to use. No sport, no strategy. Default strategy is not currently supported
    # Strategies following a naming convention: SportName + Classifier, e.g. "HockeyClassifier" or "FigureSkatingClassifier"
    if (sport == None or sport == ""):
        raise MissingSportException()
    try: 
        return getattr(sys.modules[__name__], "% sClassifier" % sport)
    except:
        # A sport was provided, but a strategy was not found. It could either not be matched or not supported
        raise UnsupportedSportException(sport)